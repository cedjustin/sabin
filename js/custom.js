// login app
var app = angular.module('login', []);
app.controller('loginCtrl', function ($scope, $http, $window) {
    $scope.checkifloggedin = () => {
        data = localStorage.getItem('logginKey');
        if (data == 'staylogged') {
            $window.location.href = "../dashboard/dashboard.html";
        } else {
            localStorage.removeItem('logginKey');
        }
    }
    $scope.login = () => {
        if ($scope.username == undefined || $scope.password == undefined) {
        }
        else {
            let config = {
                method: 'POST',
                url: 'https://sabin-app.herokuapp.com/api/signin',
                data: {
                    'username': $scope.username,
                    'password': $scope.password
                },
                headers: {
                    'id': 'sabin-1098990'
                }
            };
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    Swal({
                        type: 'error',
                        text: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else if (res.data.error == 0 && $scope.remember == true) {
                    localStorage.setItem('logginKey', 'staylogged');
                    $window.location.href = "../dashboard/dashboard.html";
                } else {
                    localStorage.setItem('logginKey', 'logged');
                    $window.location.href = "../dashboard/dashboard.html";
                }
            }).catch(e => { })
        }
    }
    $scope.checkifloggedin();
});

// dashboard
var app = angular.module('dashboard', []);
app.controller('dashCtrl', function ($scope, $http, $window) {
    $scope.checkifloggedin = () => {
        data = localStorage.getItem('logginKey');
        if (data == 'staylogged') {

        } else if (data == 'donelogged') {
            localStorage.removeItem('logginKey');
            $window.location.href = "../login/index.html";
        } else if (data == 'logged') {
            localStorage.setItem('logginKey', 'donelogged');
        } else {
            localStorage.removeItem('logginKey');
            $window.location.href = "../login/index.html";
        }
    }
    $scope.logout = () => {
        localStorage.removeItem('logginKey');
        $window.location.href = "../login/index.html";
    }
    id = 'sabin-1098990';
    $scope.getUserInfo = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getUserInfo',
            headers: {
                'id': id
            }
        };
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                Swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.userInfo = res.data.data
            }
        }).catch(e => { })
    }

    // get categories
    $scope.getCategories = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getCategories',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                console.log(res.data.data);
                $scope.categories = res.data.data;
            }
        })
    }

    // add a category
    $scope.addCategory = () => {
        if ($scope.catname == undefined || $scope.catcolor == undefined || $scope.catlink == undefined || $scope.catcaption == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'POST',
                url: 'https://sabin-app.herokuapp.com/api/addCategory',
                headers: {
                    id: id
                },
                data: {
                    'name': $scope.catname,
                    'color': $scope.catcolor,
                    'link': $scope.catlink,
                    'caption': $scope.catcaption
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        title: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.categories = res.data.data;
                }
            })
        }
    }

    // delete categories
    $scope.delCategory = (catid) => {
        Swal({
            title: 'Are you sure?',
            text: "You wont be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ed6080',
            cancelButtonColor: '#343a40',
            confirmButtonText: 'delete'
        }).then((result) => {
            if (result.value) {
                let config = {
                    method: 'DELETE',
                    url: 'https://sabin-app.herokuapp.com/api/delCategory/' + catid,
                    headers: {
                        id: id
                    }
                };
                let request = $http(config);
                request.then((res) => {
                    if (res.data.error == 1) {
                        swal({
                            type: 'error',
                            title: res.data.message,
                            confirmButtonColor: '#343a40',
                        })
                    } else {
                        swal({
                            type: 'success',
                            title: 'done',
                            confirmButtonColor: '#343a40',
                        })
                        $scope.categories = res.data.data;
                    }
                })
            }
        })
    }

    $scope.getPhotosByCat = (category) => {
        $scope.selcatname = category;
        $scope.reqloading = true;
        $scope.reqresp = false;
        let config = {
            method: 'POST',
            url: 'https://sabin-app.herokuapp.com/api/getPicByCat',
            headers: {
                id: id
            },
            data: {
                category: category
            }
        }
        let request = $http(config);
        request.then((res) => {
            $scope.reqloading = false
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.reqresp = true;
                console.log(res.data.data);
                $scope.photos = res.data.data;
            }
        })
    }

    $scope.selCategory = (id, name, caption, color, link) => {
        $scope.selcatname = name;
        $scope.selcatcaption = caption;
        $scope.selcatcolor = color;
        $scope.selcatlink = link;
        $scope.selcatid = id;
    }

    $scope.updCategory = () => {
        if ($scope.selcatname == undefined || $scope.selcatcaption == undefined || $scope.selcatcolor == undefined || $scope.selcatlink == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'PUT',
                url: 'https://sabin-app.herokuapp.com/api/updCategory',
                headers: {
                    id: id
                },
                data: {
                    'name': $scope.selcatname,
                    'color': $scope.selcatcolor,
                    'link': $scope.selcatlink,
                    'caption': $scope.selcatcaption,
                    'id': $scope.selcatid
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        title: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.categories = res.data.data;
                }
            })
        }
    }

    $scope.addPicture = () => {
        if ($scope.imagelink == undefined || $scope.imagecaption == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'POST',
                url: 'https://sabin-app.herokuapp.com/api/addPicture',
                headers: {
                    id: id
                },
                data: {
                    'imagelink': $scope.imagelink,
                    'caption': $scope.imagecaption,
                    'category': $scope.selcatname,
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        title: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.categories = res.data.data;
                }
            })
        }
    }

    $scope.updPicture = () => {
        console.log($scope.selcatname == undefined ? $scope.selimagecategory : $scope.selcatname);
        if ($scope.selimagelink == undefined || $scope.selimagecaption == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'PUT',
                url: 'https://sabin-app.herokuapp.com/api/updPicture',
                headers: {
                    id: id
                },
                data: {
                    'imagelink': $scope.selimagelink,
                    'caption': $scope.selimagecaption,
                    'imageid': $scope.selimageid,
                    'category': $scope.selcatname == undefined ? $scope.selimagecategory : $scope.selcatname
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        title: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.getHomePagePhotos();
                    $scope.getPhotosByCat($scope.selcatname == undefined ? $scope.selimagecategory : $scope.selcatname);
                    $scope.photos = res.data.data;
                }
            })
        }
    }

    $scope.delFromHomePage = (photoid) => {
        let config = {
            method: 'PUT',
            url: 'https://sabin-app.herokuapp.com/api/delFromHome',
            headers: {
                id: id
            },
            data: {
                photoid: photoid,
                category: $scope.selcatname == undefined ? $scope.selimagecategory : $scope.selcatname
            }
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.getHomePagePhotos();
                $scope.photos = res.data.data;
            }
        })
    }

    $scope.addPictureToHome = (photoid, homepage, selphtcat) => {
        $scope.selimagecategory = $scope.selimagecategory == undefined ? selphtcat : $scope.selimagecategory
        if (homepage == 1) {
            $scope.delFromHomePage(photoid);
        } else {
            let config = {
                method: 'PUT',
                url: 'https://sabin-app.herokuapp.com/api/addToHome',
                headers: {
                    id: id
                },
                data: {
                    photoid: photoid,
                    category: $scope.selcatname == undefined ? $scope.selimagecategory : $scope.selcatname
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        text: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    $scope.getHomePagePhotos();
                    $scope.photos = res.data.data;
                }
            })
        }
    }

    $scope.delPhoto = (photoid) => {
        Swal({
            title: 'Are you sure?',
            text: "You wont be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ed6080',
            cancelButtonColor: '#343a40',
            confirmButtonText: 'delete'
        }).then((result) => {
            if (result.value) {
                let config = {
                    method: 'DELETE',
                    url: 'https://sabin-app.herokuapp.com/api/delPhoto/' + photoid + '/category/' + $scope.selcatname,
                    headers: {
                        id: id
                    }
                };
                let request = $http(config);
                request.then((res) => {
                    if (res.data.error == 1) {
                        swal({
                            type: 'error',
                            title: res.data.message,
                            confirmButtonColor: '#343a40',
                        })
                    } else {
                        swal({
                            type: 'success',
                            title: 'done',
                            confirmButtonColor: '#343a40',
                        })
                        $scope.photos = res.data.data;
                        $scope.getCategories();
                    }
                })
            }
        })
    }

    $scope.getHomePagePhotos = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getHpPhotos',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.hpPhotos = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.hpPhotos = res.data.data;
            }
        })
    }

    $scope.selPhoto = (id, selimagecaption, selimagelink, selimagecategory) => {
        $scope.selimageid = id;
        $scope.selimagecaption = selimagecaption;
        $scope.selimagelink = selimagelink;
        $scope.selimagecategory = selimagecategory;
    }

    $scope.getTestimonies = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getTestimonies',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.testimonies = res.data.data;
            }
        })
    }

    $scope.addTestimony = () => {
        if ($scope.tposition == undefined || $scope.tnames == undefined || $scope.tcompany == undefined || $scope.tcaption == undefined || $scope.timagelink == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'POST',
                url: 'https://sabin-app.herokuapp.com/api/addTestimony',
                headers: {
                    id: id
                },
                data: {
                    'imageurl': $scope.timagelink,
                    'job': $scope.tposition,
                    'names': $scope.tnames,
                    'company': $scope.tcompany,
                    'caption': $scope.tcaption
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        text: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.testimonies = res.data.data;
                }
            })
        }
    }

    $scope.updTestimony = () => {
        if ($scope.selTposition == undefined || $scope.selTnames == undefined || $scope.selTcompany == undefined || $scope.selTcaption == undefined || $scope.selTimagelink == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'PUT',
                url: 'https://sabin-app.herokuapp.com/api/updTestimony',
                headers: {
                    id: id
                },
                data: {
                    'imageurl': $scope.selTimagelink,
                    'job': $scope.selTposition,
                    'names': $scope.selTnames,
                    'company': $scope.selTcompany,
                    'caption': $scope.selTcaption,
                    'testid': $scope.selTid
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    swal({
                        type: 'error',
                        text: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.testimonies = res.data.data;
                }
            })
        }
    }

    $scope.delTestimony = (tid) => {
        Swal({
            title: 'Are you sure?',
            text: "You wont be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ed6080',
            cancelButtonColor: '#343a40',
            confirmButtonText: 'delete'
        }).then((result) => {
            if (result.value) {
                let config = {
                    method: 'DELETE',
                    url: 'https://sabin-app.herokuapp.com/api/delTestimony/' + tid,
                    headers: {
                        id: id
                    }
                };
                let request = $http(config);
                request.then((res) => {
                    if (res.data.error == 1) {
                        swal({
                            type: 'error',
                            title: res.data.message,
                            confirmButtonColor: '#343a40',
                        })
                        if (res.data.message == 'there are no testimonies') {
                            $scope.testimonies = '';
                        } else {

                        }
                    } else {
                        swal({
                            type: 'success',
                            title: 'done',
                            confirmButtonColor: '#343a40',
                        })
                        $scope.testimonies = res.data.data;
                    }
                })
            }
        })
    }

    $scope.selTestimony = (tnames, timagelink, tposition, tcompany, tcaption, tid) => {
        $scope.selTnames = tnames;
        $scope.selTimagelink = timagelink;
        $scope.selTposition = tposition;
        $scope.selTcompany = tcompany;
        $scope.selTcaption = tcaption;
        $scope.selTid = tid;
    }

    $scope.getBio = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getBio',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.bio = res.data.data[0];
                $scope.biome = res.data.data[0].me
                $scope.biost = res.data.data[0].stsession
                $scope.bioot = res.data.data[0].otsession
                $scope.biocl = res.data.data[0].clients
                $scope.biobio = res.data.data[0].bio
                $scope.bioimagelink = res.data.data[0].imagelink
                $scope.biop = res.data.data[0].pounds
            }
        })
    }

    $scope.updBio = () => {
        let config = {
            method: 'PUT',
            url: 'https://sabin-app.herokuapp.com/api/updBio',
            headers: {
                id: id
            },
            data: {
                'imageurl': $scope.bioimagelink,
                'stsession': $scope.biost,
                'names': $scope.biome,
                'otsession': $scope.bioot,
                'pounds': $scope.biop,
                'clients': $scope.biocl,
                'bio': $scope.biobio
            }
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                swal({
                    type: 'success',
                    title: 'done',
                    confirmButtonColor: '#343a40',
                })
                $scope.bio = res.data.data[0];
            }
        })
    }

    $scope.getContact = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getContact',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.contacts = res.data.data[0];
                $scope.cstreet = res.data.data[0].street
                $scope.cdistrict = res.data.data[0].district
                $scope.ccity = res.data.data[0].city
                $scope.cemail = res.data.data[0].email
                $scope.cphone = res.data.data[0].phone
                $scope.cdesc = res.data.data[0].description
            }
        })
    }

    $scope.updContact = () => {
        let config = {
            method: 'PUT',
            url: 'https://sabin-app.herokuapp.com/api/updContact',
            headers: {
                id: id
            },
            data: {
                'email': $scope.cemail,
                'phone': $scope.cphone,
                'street': $scope.cstreet,
                'district': $scope.cdistrict,
                'city': $scope.ccity,
                'description': $scope.cdesc,
            }
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                swal({
                    type: 'success',
                    title: 'done',
                    confirmButtonColor: '#343a40',
                })
                $scope.contacts = res.data.data[0];
            }
        })
    }

    $scope.getSM = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getSM',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.sms = res.data.data;
            }
        })
    }

    $scope.addSM = () => {
        if ($scope.smname == undefined || $scope.smlink == undefined) {
            swal({
                type: 'warning',
                title: "Please provide all info",
                confirmButtonColor: '#343a40',
            })
        } else {
            let config = {
                method: 'POST',
                url: 'https://sabin-app.herokuapp.com/api/addSM',
                headers: {
                    id: id
                },
                data: {
                    'name': $scope.smname,
                    'link': $scope.smlink
                }
            }
            let request = $http(config);
            request.then((res) => {
                if (res.data.error == 1) {
                    $scope.testimonies = '';
                    swal({
                        type: 'error',
                        text: res.data.message,
                        confirmButtonColor: '#343a40',
                    })
                } else {
                    swal({
                        type: 'success',
                        title: 'done',
                        confirmButtonColor: '#343a40',
                    })
                    $scope.sms = res.data.data;
                }
            })
        }
    }

    $scope.updSM = () => {
        let config = {
            method: 'PUT',
            url: 'https://sabin-app.herokuapp.com/api/updSM',
            headers: {
                id: id
            },
            data: {
                'name': $scope.selsmname,
                'link': $scope.selsmlink,
                'smid': $scope.selsmid,
            }
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                swal({
                    type: 'success',
                    title: 'done',
                    confirmButtonColor: '#343a40',
                })
                $scope.sms = res.data.data;
            }
        })
    }

    $scope.selSM = (id, name, link) => {
        $scope.selsmname = name;
        $scope.selsmlink = link;
        $scope.selsmid = id;
    }

    $scope.delSM = (smid) => {
        Swal({
            title: 'Are you sure?',
            text: "You wont be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ed6080',
            cancelButtonColor: '#343a40',
            confirmButtonText: 'delete'
        }).then((result) => {
            if (result.value) {
                let config = {
                    method: 'DELETE',
                    url: 'https://sabin-app.herokuapp.com/api/delSM/' + smid,
                    headers: {
                        id: id
                    }
                }
                let request = $http(config);
                request.then((res) => {
                    if (res.data.error == 1) {
                        swal({
                            type: 'error',
                            text: res.data.message,
                            confirmButtonColor: '#343a40',
                        })
                    } else {
                        swal({
                            type: 'success',
                            title: 'done',
                            confirmButtonColor: '#343a40',
                        })
                        $scope.sms = res.data.data;
                    }
                })
            }
        })
    }

    // method calls
    $scope.getSM();
    $scope.getContact();
    $scope.getBio();
    $scope.getTestimonies();
    $scope.getHomePagePhotos();
    $scope.checkifloggedin();
    $scope.getCategories();
});

// homepage
var app = angular.module('home', []);
app.controller('homeCtrl', function ($scope, $http, $window) {
    id = 'sabin-1098990';
    $scope.getHomePagePhotos = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getHpPhotos',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.hpPhotos = '';
            } else {
                $scope.hpPhotos = res.data.data;
            }
        })
    }
    $scope.getTestimonies = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getTestimonies',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
            } else {
                $scope.testimonies = res.data.data;
            }
        })
    }

    $scope.getBio = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getBio',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.bio = res.data.data[0];
            }
        })
    }

    $scope.getContact = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getContact',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.contacts = res.data.data[0];
            }
        })
    }

    $scope.getSM = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getSM',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                $scope.testimonies = '';
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                $scope.sms = res.data.data;
            }
        })
    }

    // get categories
    $scope.getCategories = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getCategories',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                console.log(res.data.data);
                $scope.categories = res.data.data;
            }
        })
    }

    // get photos
    $scope.getphotos = () => {
        let config = {
            method: 'GET',
            url: 'https://sabin-app.herokuapp.com/api/getPictures',
            headers: {
                id: id
            },
        }
        let request = $http(config);
        request.then((res) => {
            if (res.data.error == 1) {
                swal({
                    type: 'error',
                    text: res.data.message,
                    confirmButtonColor: '#343a40',
                })
            } else {
                console.log(res.data.data);
                $scope.photos = res.data.data;
            }
        })
    }

    $scope.setCategory = (cat)=>{
        if(cat==undefined){
            $scope.category = ''
        } else {
            $scope.category = cat;
        }
    }

    // method calls
    $scope.category='';
    $scope.getphotos();
    $scope.getCategories();
    $scope.getSM();
    $scope.getContact();
    $scope.getBio();
    $scope.getTestimonies();
    $scope.getHomePagePhotos();
});